<?php

namespace Tests\Unit;

use App\Classes\Airlines\AirIndiaHtmlCrewRoster;
use App\Models\Flight;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

const ROSTER_DISK = 'rosters';
const ROSTER = 'EXAMPLE ROSTER.html';

class RosterTest extends TestCase
{
    /**
     * @var \App\Classes\Airlines\AirIndiaHtmlCrewRoster
     */
    public $roster;

    public function testReadFile()
    {
        $this->assertTrue(Storage::disk(ROSTER_DISK)->exists(ROSTER));
    }

    public function testExtractDatesFromHeaders()
    {
        $dates = $this->roster->getDates();
        $this->assertInstanceOf(Collection::class, $dates);

        $from = $dates->first();
        $this->assertInstanceOf(Carbon::class, $from);

        $to = $dates->last();
        $this->assertInstanceOf(Carbon::class, $to);
    }

    public function testGetTimezoneFromHeader()
    {
        $timezone = $this->roster->getTimezone();
        $this->assertIsString($timezone);
        $this->assertStringStartsWith('UTC', $timezone);
    }

    public function testGetDaysFromRoster()
    {
        $days = $this->roster->getDaysInRoster();
        $this->assertCount(31, $days);

        // Check first day in roster
        $this->assertEquals('May', $days[0][0]);
        $this->assertEquals('29', $days[0][1]);
        $this->assertEquals('Tue', $days[0][2]);

        // Check random other day in roster
        $this->assertEquals('Jun', $days[6][0]);
        $this->assertEquals('04', $days[6][1]);
        $this->assertEquals('Mon', $days[6][2]);
    }

    public function testGetSpecificDayFromRoster()
    {
        $day = $this->roster->getDay(30);

        $this->assertEquals('Jun', $day[0]);
        $this->assertEquals('28', $day[1]);
        $this->assertEquals('Thu', $day[2]);
    }

    public function testGetCrewNameFromRoster()
    {
        $this->assertEquals('Holy Elon', $this->roster->getCrewName());
    }

    public function testGetCrewIdFromRoster()
    {
        $this->assertEquals('12345 (Lgw Fa-319,320 )', $this->roster->getCrewId());
    }

    public function testGetDaysOff()
    {
        $this->assertEquals([
            'May29Tue',
            'Jun03Sun',
            'Jun04Mon',
            'Jun05Tue',
            'Jun06Wed',
            'Jun07Thu',
            'Jun10Sun',
            'Jun11Mon',
            'Jun16Sat',
            'Jun17Sun',
            'Jun23Sat',
            'Jun24Sun',
            'Jun25Mon',
        ], $this->roster->getDaysOffInRoster()->toArray());
    }

    public function testGetEarlyStandbyInRoster()
    {
        $this->assertCount(2, $this->roster->getEarlyStandbyInRoster());
        $this->assertEquals([
            [
                'day' => 'May30Wed',
                'from' => '06:00',
                'until' => '14:00',
            ],
            [
                'day' => 'Jun12Tue',
                'from' => '05:15',
                'until' => '13:15',
           ]
        ], $this->roster->getEarlyStandbyInRoster());
    }

    public function testGetFlightsInRoster()
    {
        $this->assertCount(38, $this->roster->getFlightsInRoster());
        $this->assertInstanceOf(Flight::class, $this->roster->getFlightsInRoster()->first());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->roster = new AirIndiaHtmlCrewRoster(ROSTER, ROSTER_DISK);
    }
}
