<?php

namespace App\Classes;

use App\Models\Flight;
use Carbon\Carbon;
use DOMDocument;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

abstract class HtmlRoster
{
    /**
     * @var \DOMDocument
     */
    protected DOMDocument $document;

    /**
     * @var string
     */
    protected string $roster_path = 'app/rosters/';

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function __construct(string $filename, string $disk)
    {
        $html = $this->getValidatedHtml($filename, $disk);
        $this->document = new DOMDocument();
        $this->document->loadHTML($html);
    }

    /**
     * Check for misplaced DOCTYPE declaration in Entity
     * In this case the document has a style element on the first line
     * and the html element on the second line. We strip the first line
     * and overwrite te roster-file to the storage.
     *
     * @param $filename
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getValidatedHtml($filename, $disk): string
    {
        $file = file(storage_path($this->roster_path).$filename);
        if (str_contains($file[0], '<style>')) {
            unset($file[0]);
            Storage::disk($disk)->put($filename, $file);
        }

        return Storage::disk($disk)->get($filename);
    }

    /**
     * @return string
     */
    public function getTimezone(): string
    {
        $element = $this->getElementsWithContent('Period: ', 'td')->first();
        preg_match_all('/(UTC(\+|-?)(\d+)?)/', $element->textContent, $matches);

        return collect($matches)->flatten()->first();
    }

    /**
     * @param string $content
     * @return \Illuminate\Support\Collection
     */
    protected function getElementsWithContent(string $content, string $element): Collection
    {
        $columns = $this->document->getElementsByTagName($element);

        return collect($columns)->filter(function ($el) use ($content) {
            return ! empty($el->textContent) && str_contains($el->textContent, $content);
        });
    }

    /**
     * @param \DOMElement $element
     * @return \Illuminate\Support\Collection
     */
    public function getDates(string $format = 'd/m/Y'): Collection
    {
        $element = $this->getElementsWithContent('Period: ', 'td')->first();

        preg_match_all('/(\d{2})\/(\d{2})\/(\d{4})/', $element->textContent, $matches);
        $dates = $matches[0];

        return collect($dates)->map(function ($date) use ($format) {
            return Carbon::createFromFormat($format, $date);
        });
    }

    /**
     * @return string
     */
    public function getCrewName(): string
    {
        $element = $this->getElementsWithContent('NAME ', 'td')->filter(function ($item) {
            return str_contains($item->textContent, 'ID ');
        })->first();

        $value = $element->childNodes->item(0)->textContent;
        if (! $value) {
            return '';
        }
        $value = HtmlRoster::cleanString($value);

        return Str::title(trim(explode(':', $value)[1]));
    }

    /**
     * @param string $subject
     * @return string
     */
    private static function cleanString(string $subject): string
    {
        $subject = preg_replace('/ /', ' ', $subject);

        return trim($subject);
    }

    /**
     * @return string
     */
    public function getCrewId()
    {
        $element = $this->getElementsWithContent('NAME ', 'td')->filter(function ($item) {
            return str_contains($item->textContent, 'ID ');
        })->first();

        $value = $element->childNodes->item(2)->textContent;
        if (! $value) {
            return '';
        }
        $value = HtmlRoster::cleanString($value);

        return Str::title(trim(explode(':', $value)[1]));
    }

    /**
     * @param int $row_index
     * @return array
     */
    public function getDaysInRoster(int $row_index = 5): array
    {
        $row = $this->document->getElementsByTagName('tr')->item($row_index);

        return collect($row->getElementsByTagName('td'))->map(function ($day, $key) use ($row_index) {
            return $this->getDay($key, $row_index);
        })->filter(function ($day) {
            return ! empty($day);
        })->toArray();
    }

    /**
     * @param int $column_index
     * @param int $row_index
     * @return array
     */
    public function getDay(int $column_index = 0, int $row_index = 5)
    {
        $dates = $this->document->getElementsByTagName('tr')[$row_index];
        preg_match('/^(\w{3})(\d{2})(\w{3})$/', $dates->getElementsByTagName('td')
                                                      ->item($column_index)->textContent, $day);
        unset($day[0]);

        return array_values($day);
    }

    /**
     * @param int $row_index
     * @return \Illuminate\Support\Collection
     */
    public function getDaysOffInRoster($row_index = 6): Collection
    {
        $row = $this->document->getElementsByTagName('tr')->item($row_index);

        return collect($row->getElementsByTagName('td'))->filter(function ($column) {
            // Filter the 'D/O' values
            return str_contains($column->textContent, 'D/O');
        })->map(function ($value, $key) {
            // Get the Days assigned to the days off via the keys.
            return join('', $this->getDay($key));
        })->values();
    }

    /**
     * @param int $row_index
     * @return array
     */
    public function getEarlyStandbyInRoster($row_index = 6): array
    {
        $row = $this->getRow($row_index);

        return collect($row->getElementsByTagName('td'))->filter(function ($column) {
            // Filter the 'ESBY' values
            return str_contains($column->textContent, 'ESBY');
        })->map(function ($item, $key) use ($row_index) {
            // Get the Days assigned early standby via the keys.
            return $this->getEarlyStandbyData($key, $row_index);
        })->values()->toArray();
    }

    /**
     * @param int $row_index
     * @return \DOMNode|null
     */
    private function getRow(int $row_index)
    {
        return $this->document->getElementsByTagName('tr')->item($row_index);
    }

    /**
     * @param int $column_index
     * @param int $row_index
     * @return array
     */
    public function getEarlyStandbyData(int $column_index, $row_index = 6)
    {
        return [
            'day' => join('', $this->getDay($column_index)),
            'from' => $this->getColumnData(($row_index + 1), $column_index),
            'until' => $this->getColumnData(($row_index + 2), $column_index),
        ];
    }

    /**
     * @param $row
     * @param $column
     * @return mixed
     */
    protected function getColumnData($row, $column)
    {
        $row = $this->getRow($row);

        return $row->getElementsByTagName('td')->item($column)->textContent;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getFlightsInRoster(): Collection
    {
        // Read all rows
        return collect($this->document->getElementsByTagName('tr'))->flatMap(function ($row, $row_index) {
            // Get columns in row.
            return collect($row->getElementsByTagName('td'))->filter(function ($column) {
                // Filter all columns that do not have a valid flight number.
                return preg_match('/^(\d+)$/', $column->textContent);
            })->map(function ($column, $key) use ($row_index) {
                // Get flight data by row and column indexes and return a new Flight Model instance.
                return new Flight($this->getFlightData($key, $row_index));
            });
        })->values();
    }

    /**
     * @param int $column_index
     * @param int $row_index
     * @return array
     */
    public function getFlightData(int $column_index, int $row_index = 6)
    {
        return [
            'date' => $this->getDate($column_index),
            'flight_number' => $this->getFlightNumber($row_index, $column_index),
            'report_time' => $this->getReportTime($row_index, $column_index),
            'departure_time' => $this->getDepartureTime($row_index, $column_index),
            'departure_airport' => $this->getDepartureAirport($row_index, $column_index),
            'arrival_airport' => $this->getArrivalAirport($row_index, $column_index),
            'arrival_time' => $this->getArrivalTime($row_index, $column_index),
        ];
    }

    /**
     * @param $column_index
     * @return string
     */
    protected function getDate($column_index)
    {
        return join('', $this->getDay($column_index));
    }

    /**
     * @param int $row_index
     * @param int $column_index
     * @return mixed
     */
    protected function getFlightNumber(int $row_index, int $column_index)
    {
        return $this->getColumnData($row_index, $column_index);
    }

    /**
     * @param int $row_index
     * @param int $column_index
     * @return null
     */
    protected function getReportTime(int $row_index, int $column_index)
    {
        return $row_index == 6 ? $this->getColumnData($row_index + 1, $column_index) : null;
    }

    /**
     * @param int $row_index
     * @param int $column_index
     * @return mixed
     */
    protected function getDepartureTime(int $row_index, int $column_index)
    {
        return $row_index == 6 ? $this->getColumnData($row_index + 2, $column_index) : $this->getColumnData($row_index + 1, $column_index);
    }

    /**
     * @param int $row_index
     * @param int $column_index
     * @return mixed
     */
    protected function getDepartureAirport(int $row_index, int $column_index)
    {
        return $row_index == 6 ? $this->getColumnData($row_index + 3, $column_index) : $this->getColumnData($row_index + 2, $column_index);
    }

    /**
     * @param int $row_index
     * @param int $column_index
     * @return mixed
     */
    protected function getArrivalAirport(int $row_index, int $column_index)
    {
        return $row_index == 6 ? $this->getColumnData($row_index + 4, $column_index) : $this->getColumnData($row_index + 3, $column_index);
    }

    /**
     * @param int $row_index
     * @param int $column_index
     * @return mixed
     */
    protected function getArrivalTime(int $row_index, int $column_index)
    {
        return $row_index == 6 ? $this->getColumnData($row_index + 5, $column_index) : $this->getColumnData($row_index + 4, $column_index);
    }
}