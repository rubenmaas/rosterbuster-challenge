<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $fillable = [
        'date',
        'flight_number',
        'report_time',
        'departure_time',
        'departure_airport',
        'arrival_airport',
        'arrival_time',
    ];
}
