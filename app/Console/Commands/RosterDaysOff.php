<?php

namespace App\Console\Commands;

use App\Classes\Airlines\AirIndiaHtmlCrewRoster;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class RosterDaysOff extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roster:days-off {--file=} {--disk=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $roster_disk = $this->option('disk') ? $this->option('disk') : config('roster.disk');
            $roster_file = $this->option('file') ? $this->option('file') : config('roster.default_file');
            $roster = new AirIndiaHtmlCrewRoster($roster_file, $roster_disk);
            $days_off = $roster->getDaysOffInRoster();

            $this->output->title("Days off duty for {$roster->getCrewName()}");
            $this->line('');
            $this->output->block($days_off->toJson(JSON_PRETTY_PRINT));
            $this->line('');
            $this->info("We've found a total of {$days_off->count()} days off in {$roster_file}");
            $this->info("All times in this roster are in {$roster->getTimezone()}");

            return 0;
        } catch (FileNotFoundException $e) {
            $this->output->error($e->getMessage());
        }
    }
}
