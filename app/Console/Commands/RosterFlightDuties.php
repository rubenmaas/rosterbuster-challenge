<?php

namespace App\Console\Commands;

use App\Classes\Airlines\AirIndiaHtmlCrewRoster;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class RosterFlightDuties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roster:duties {--file=} {--disk=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all flight duties';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $roster_disk = $this->option('disk') ? $this->option('disk') : config('roster.disk');
            $roster_file = $this->option('file') ? $this->option('file') : config('roster.default_file');

            $roster = new AirIndiaHtmlCrewRoster($roster_file, $roster_disk);
            $flights = $roster->getFlightsInRoster();

            // Output
            $this->output->title("Flight duties for {$roster->getCrewName()}");
            $this->line('');
            $this->output->block($flights->groupBy('date')->sortBy('report_time')->toJson(JSON_PRETTY_PRINT));
            $this->line('');

            $this->info("We've found a total of {$flights->count()} flight duties in {$flights->groupBy('date')->count()} days in the file {$roster_file}");
            $this->info("All times in this roster are in {$roster->getTimezone()}");
            return 0;

        } catch (FileNotFoundException $e) {
            $this->output->error($e->getMessage());
        }

    }
}
