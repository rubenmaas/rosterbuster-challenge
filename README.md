# RosterBuster Challenge

### Installation
1. clone this repository
2. `cd [folder_name]`
3. Run `composer install` in the project root directory

### Testing
Run `php artisan test` in the project root directory

### The Assignment
See below a screenshot of a roster for Air India crew. We have attached the source html.
Create a standalone PHP program, executable from command line, which: 

1. Extracts all ​flight duties ​from the provided source HTML.
2. Transforms the extracted data into ‘Flight’ models.
3. Outputs the result as JSON to the console or browser.
4. Validates the output with a test

We expect a list of flight model objects in json. Check: there should be ​38 flight duties​ on the roster. Other information on the roster may be ignored, but we appreciate it if you manage to extract other information from the roster’s source, e.g. Day Off (D/O).

### Available Commands

#### Roster duties
`php artisan roster:duties`

```
// optional flags:
- --disk=   // The disk to use (default: "rosters").
- --file=   // The file to parse (default: "EXAMPLE ROSTER.html")
```

#### Roster Days Off
`php artisan roster:days-off`

```
// optional flags:
- --disk=   // The disk to use (default: "rosters").
- --file=   // The file to parse (default: "EXAMPLE ROSTER.html")
```
