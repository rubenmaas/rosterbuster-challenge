<?php

/**
 * Roster configuration
 */
return [
    'default_file' => env('ROSTER_FILE', 'EXAMPLE ROSTER.html'),
    'disk' => env('ROSTER_DISK', 'rosters')
];